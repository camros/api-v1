"""Models of Account App"""
from datetime import timedelta
import uuid
from django.contrib.auth.base_user import AbstractBaseUser
from django.conf import settings
from django.utils import timezone
from rest_framework.authtoken.models import Token
from django.db import models


class User(AbstractBaseUser):
    """
    Account Table
    """
    class Meta:
        ordering = ["-created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(max_length=200, unique=True)
    address = models.CharField(max_length=1000)
    location = models.JSONField()

    created_at = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)
    last_modified = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = "email"
    EmailField = "email"
    REQUIRED_FIELDS = ['location', 'address', 'password']


class AccessToken(Token):
    """
    Authorization Token Model

    """

    def expire_time_func():
        return timezone.now() + timedelta(seconds=settings.TOKEN_AUTH_EXPIRED_TIME)

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    expire_time = models.DateTimeField(default=expire_time_func)
    is_active = models.BooleanField(default=True)
    # Hash of user agent request -> Default is hash of b''
    user_agent = models.CharField(max_length=150,
                                  default="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855")
    #
    origin = models.CharField(
        max_length=200, default="https://matna.pnete.com")
    created_at = models.DateTimeField(
        default=timezone.now)
    last_modified = models.DateTimeField(
        auto_now=True)

    created_by = models.ForeignKey(
        "account.User",
        null=True,
        blank=True,
        related_name="+",
        on_delete=models.SET_NULL
    )

    modified_by = models.ForeignKey(
        "account.User",
        null=True,
        blank=True,
        related_name="+",
        on_delete=models.SET_NULL
    )

    def isExpire(self):
        now = timezone.now()
        if self.expire_time <= now:
            return True
        if settings.EXPIRE_TOKEN_WHEN_USER_ACTIVE:
            if now - self.created_at >= timedelta(days=7):
                return True
        if not self.is_active:
            return True
        return False

    def update(self):
        self.expire_time = timezone.now() + timedelta(seconds=settings.TOKEN_AUTH_EXPIRED_TIME)
        self.save()
