"""Models of Product App"""
import uuid
from django.utils import timezone
from django.db import models
from rest_framework import exceptions


class Product(models.Model):
    """
    Product Table
    """
    class Meta:
        ordering = ["-created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=500)
    description = models.TextField(null=True)
    # TODO: As we dont have a file management solution we just store a link,
    #       but photo (or any files) should be forien key of file table.
    photo = models.CharField(max_length=500, null=True)
    price = models.DecimalField(max_digits=12, decimal_places=2)

    created_at = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)
    last_modified = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_or_404(pk):
        product = Product.objects.filter(pk=pk).first()
        if not product:
            raise exceptions.NotFound("Product Not Found!")
        return product