from rest_framework import generics
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from product.pagination import MediumResultsSetPagination
from .models import Product
from config.logger import LoggerMixin
from .serializers import ProducSerializer
from django_filters import rest_framework as filters
from rest_framework import filters as rest_filter


class ProductView(LoggerMixin, ListCreateAPIView):
    """
    List and Create Product
    """
    serializer_class = ProducSerializer
    pagination_class = MediumResultsSetPagination
    filter_backends = [filters.DjangoFilterBackend, rest_filter.SearchFilter]
    search_fields = ["product__title"]
    permission_classes = []
    name = "product"
    # TODO: After implementation of role managements, product creation access should be checked.
    
    def get_queryset(self):
        return Product.objects.all().filter(active=True)


class ProductDetailView(LoggerMixin, RetrieveUpdateAPIView):
    """
    Get and Update Product
    """
    serializer_class = ProducSerializer
    permission_classes = []
    name = "product_details"

    def get_queryset(self):
        return Product.objects.filter(active=True)