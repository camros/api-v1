"""Product Test Module"""
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from .models import Product


class ProductTest(TestCase):
    """Test Product Model, View, ..."""

    def setUp(self):
        self.product_data = {
            "title": 'product 1',
            "description": 'The best price with 40 percent off',
            "photo": "https://images.pexels.com/photos/90946/pexels-photo-90946.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
            "price": 12.5
        }

    def test_001_create_product(self):
        """
        Create product test
        """
        client = Client()

        response = client.post(
            reverse('product:product'), data=self.product_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        user_obj = Product.objects.get(title=self.product_data["title"])
        self.assertEqual(user_obj.title, self.product_data['title'])

    def test_002_create_product_failed(self):
        """
        Create product - failed scenarios
        """
        client = Client()

        response = client.post(reverse('product:product'), data={
                               **self.product_data, "price": ""})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = client.post(reverse('product:product'), data={
                               **self.product_data, "title": ""})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_003_ge_product(self):
        """
        Get product details
        """
        client = Client()
        response = client.post(
            reverse('product:product'), data=self.product_data)
        create_product_id = response.json()['id']
        response = client.get(
            reverse('product:product_details', kwargs={'pk': create_product_id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        detail_product_id = response.json()['id']
        self.assertEqual(create_product_id, detail_product_id)
