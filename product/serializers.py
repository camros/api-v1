"""
define Serialize instance to json format
also define validator for input data
"""

from rest_framework import serializers
from django_restql.mixins import DynamicFieldsMixin
from .models import Product

class ProducSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    """Serializer For Product Model"""

    class Meta:
        model = Product
        fields = ["id", "created_at", "title", "description", "photo", "price"]
        read_only_fields = ["created_at", "id"]