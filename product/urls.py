"""Url Routes for Product App"""

from django.urls import path
from .views import (
    ProductView,
    ProductDetailView,
)

app_name = 'product'

urlpatterns = [
    path('', ProductView.as_view(), name='product'),
    path('<str:pk>', ProductDetailView.as_view(), name='product_details'),
]
