# Generated by Django 3.1.7 on 2023-01-19 13:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='last_login',
        ),
        migrations.RemoveField(
            model_name='product',
            name='password',
        ),
    ]
