"""Models of Cart App"""
import uuid
from django.utils import timezone
from django.db import models
from django.core import serializers
import json


class Cart(models.Model):
    """
    Cart Table
    """
    class Meta:
        ordering = ["-created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(
        "account.User", on_delete=models.PROTECT, related_name="cart")
    # TODO: Delivery, discount, payment type, cart status and etc can be added

    created_at = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)
    last_modified = models.DateTimeField(auto_now=True)


class CartProduct(models.Model):
    """
    CartProduct Table
    """
    class Meta:
        ordering = ["-created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    cart = models.ForeignKey(
        Cart, on_delete=models.CASCADE, related_name="cart_products")
    product = models.ForeignKey("product.Product", on_delete=models.PROTECT)
    product_count = models.PositiveIntegerField()

    created_at = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)
    last_modified = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_cart_products(cart):
        cart_products = []
        for item in CartProduct.objects.filter(active=True, cart=cart).select_related():
            product = {
                **json.loads(serializers.serialize('json', [item.product]))[0]}
            product["fields"]["id"] = product["pk"]
            cart_products.append({
                "product_count": item.product_count,
                "product": product.get("fields"),
            })
        return cart_products
