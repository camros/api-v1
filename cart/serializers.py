"""
define Serialize instance to json format
also define validator for input data
"""

from rest_framework import serializers
from django_restql.mixins import DynamicFieldsMixin
from .models import Cart, CartProduct

class CartProductSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    """Serializer For Cart Product Model"""

    class Meta:
        model = CartProduct
        fields = ["id", "created_at", "cart", "product", "product_count"]
        read_only_fields = ["created_at", "id"]


class CartSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    """Serializer For Cart Model"""

    class Meta:
        model = Cart
        fields = ["id", "created_at", "cart_products"]
        read_only_fields = ["created_at", "id"]
