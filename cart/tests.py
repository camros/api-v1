"""Cart Test Module"""
import json
from django.test import TestCase, Client, override_settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


class CartTest(TestCase):
    """Test Cart Model, View, ..."""

    @override_settings(RATELIMIT_ENABLE=False)
    def setUp(self):
        self.product_data = {
            "title": 'product 1',
            "description": 'The best price with 40 percent off',
            "photo": "https://images.pexels.com/photos/90946/pexels-photo-90946.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
            "price": 12.5
        }
        self.register_data = {
            "email": "test1@email.test",
            "address": "Address 1 Address 1 Address 1",
            "location": json.dumps("{lat: 34.5, lng: 56.3}"),
            "password": "123456",
            "confirm_password": "123456"
        }
        self.login_data = {
            "email": "test1@email.test",
            "password": "123456"
        }
        client = Client()
        product_response = client.post(
            reverse('product:product'), data=self.product_data)
        self.product = product_response.json()
        register_response = client.post(
            reverse('account:register'), data=self.register_data)
        self.user = register_response.json()

    @override_settings(RATELIMIT_ENABLE=False)
    def test_001_unauthorized_add_product_to_cart(self):
        """
        Add product to cart - Unauthorized
        """

        client = Client()
        response = client.get(
            reverse('cart:add_to_cart', kwargs={'pk': self.product["id"]}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    @override_settings(RATELIMIT_ENABLE=False)
    def test_002_add_product_to_cart(self):
        """
        Add product to cart - Success
        """

        client = Client()
        response = client.post(reverse('account:login'), data=self.login_data)
        token = response.json()['access_token']
        key = token.get("key")
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Token {key}")
        response = client.post(
            reverse('cart:add_to_cart', kwargs={'pk': self.product["id"]}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @override_settings(RATELIMIT_ENABLE=False)
    def test_003_add_product_to_cart_failed(self):
        """
        Add product to cart - Failed
        """

        client = Client()
        response = client.post(reverse('account:login'), data=self.login_data)
        token = response.json()['access_token']
        key = token.get("key")
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Token {key}")
        response = client.post(
            reverse('cart:add_to_cart', kwargs={'pk': self.user["id"]}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @override_settings(RATELIMIT_ENABLE=False)
    def test_005_remove_product_from_cart(self):
        """
        Remove product to cart - Success
        """

        client = Client()
        response = client.post(reverse('account:login'), data=self.login_data)
        token = response.json()['access_token']
        key = token.get("key")
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Token {key}")
        client.post(reverse('cart:add_to_cart',
                    kwargs={'pk': self.product["id"]}))
        client.post(reverse('cart:add_to_cart',
                    kwargs={'pk': self.product["id"]}))
        response = client.post(
            reverse('cart:remove_from_cart', kwargs={'pk': self.product["id"]}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @override_settings(RATELIMIT_ENABLE=False)
    def test_006_remove_product_from_cart(self):
        """
        Remove product to cart - Failed
        """

        client = Client()
        response = client.post(reverse('account:login'), data=self.login_data)
        token = response.json()['access_token']
        key = token.get("key")
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Token {key}")
        response = client.post(
            reverse('cart:remove_from_cart', kwargs={'pk': self.product["id"]}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @override_settings(RATELIMIT_ENABLE=False)
    def test_007_get_user_cart(self):
        """
        Get cart - Success
        """

        client = Client()
        response = client.post(reverse('account:login'), data=self.login_data)
        token = response.json()['access_token']
        key = token.get("key")
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Token {key}")
        response = client.post(
            reverse('cart:add_to_cart', kwargs={'pk': self.product["id"]}))
        response = client.get(reverse('cart:cart'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @override_settings(RATELIMIT_ENABLE=False)
    def test_008_get_user_cart_failed(self):
        """
        Get cart - Failed
        """

        client = Client()
        response = client.post(reverse('account:login'), data=self.login_data)
        token = response.json()['access_token']
        key = token.get("key")
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Token {key}")
        response = client.get(reverse('cart:cart'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
