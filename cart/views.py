from rest_framework.response import Response
from rest_framework import generics
from .models import Cart, CartProduct
from product.models import Product
from config.logger import LoggerMixin
from rest_framework import permissions, exceptions
from rest_framework.serializers import Serializer


class CartView(LoggerMixin, generics.GenericAPIView):
    """
    Get Cart
    """
    serializer_class = Serializer
    permission_classes = [permissions.IsAuthenticated]
    name = "cart"

    def get(self, request, *args, **kwargs):
        """GET Method View"""
        if not hasattr(self.request.user, 'cart'):
            raise exceptions.NotFound("Cart Not Found!")

        cart_products = CartProduct.get_cart_products(self.request.user.cart)
        return Response(data=cart_products, status=200)


class AddProductView(LoggerMixin, generics.GenericAPIView):
    """
    Add Product to Cart
    """
    serializer_class = Serializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        """Post Method View"""
        cart = self.request.user.cart if hasattr(
            self.request.user, 'cart') else self.create_cart()
        product = Product.get_or_404(kwargs["pk"])
        self.add_product_to_cart(product, cart)
        return Response(data={"detail": "Added Successfully!"}, status=200)

    def create_cart(self):
        return Cart.objects.create(
            user=self.request.user
        )

    def add_product_to_cart(self, product, cart):
        cart_product = CartProduct.objects.filter(
            product=product, cart=cart).first()

        if cart_product:
            cart_product.product_count = cart_product.product_count + 1
            cart_product.save()
            return cart_product

        return CartProduct.objects.create(
            cart=cart,
            product=product,
            product_count=1
        )


class RemoveProductView(LoggerMixin, generics.GenericAPIView):
    """
    Remove Product from Cart
    """
    serializer_class = Serializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        """Post Method View"""
        cart = self.request.user.cart if hasattr(
            self.request.user, 'cart') else None
        if not cart:
            raise exceptions.NotFound("Cart Not Found!")

        product = Product.objects.get(pk=kwargs["pk"])
        self.remove_product_from_cart(product, cart)
        return Response(data={"detail": "Removed Successfully!"}, status=200)

    def remove_product_from_cart(self, product, cart):
        cart_product = CartProduct.objects.filter(
            product=product, cart=cart).first()

        if cart_product and cart_product.product_count > 1:
            cart_product.product_count = cart_product.product_count - 1
            cart_product.save()
            return cart_product

        if cart_product:
            cart_product.delete()
            if not CartProduct.objects.filter(cart=cart).first():
                cart.delete()

        raise exceptions.NotFound("Cart Product Not Found!")
