"""Url Routes for Cart App"""

from django.urls import path
from .views import (
    CartView,
    AddProductView,
    RemoveProductView
)

app_name = 'cart'

urlpatterns = [
    path('add/product/<str:pk>', AddProductView.as_view(), name='add_to_cart'),
    path('remove/product/<str:pk>', RemoveProductView.as_view(), name='remove_from_cart'),
    path('', CartView.as_view(), name='cart'),
]
