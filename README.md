# CAMROS ONLINE SHOP API VERSION 1

## INSTALL Requirements and start app without docker (docker instructions are listed at the next section and its recommended to use docker to run the project)
- python3 -m venv .venv
- source .venv/bin/activate
- pip install -r requirements.txt
- cp .env.sample .env (edit .env file)
- python manage.py runserver
- make sure you installed and configured redis, postgres and nginx on your server

--------------------------

## StartApp by Docker
- sudo docker network create api_network 
- sudo docker volume create --name=apiv1_psql
- cp .env.sample .env (edit .env file)
- sudo docker compose up -d --build
- sudo docker exec -it apiv1 python manage.py collectstatic
- Now you should be able to see the doc page: http://0.0.0.0:9090/help-swagger

To reset the app:
- sudo docker-compose down; sudo docker-compose up -d
--------------------------

## Testing
- python manage.py test

--------------------------

## Coverage Test
- coverage run --source='.' manage.py test
- coverage report

--------------------------

### Migrate app
- sudo docker exec apiv1 python manage.py makemigrations
- sudo docker exec apiv1 python manage.py makemigrations account # Migrate specific app
- sudo docker exec apiv1 python manage.py migrate

--------------------------

### Run Shell and exec command with ipython in django shell
- sudo docker exec -it apiv1 python manage.py shell -i ipython

--------------------------

### Build Application
- sudo docker-compose build apiv1
- sudo docker-compose up -d 

--------------------------

### Restart Application 
- sudo docker-compose restart apiv1

--------------------------

### Test Application 
- sudo docker exec -it apiv1 python manage.py test
- sudo docker exec -it apiv1 python manage.py test account  # Test Specific App
- sudo docker exec -it apiv1 python manage.py test account -k 2 # Test Specific App and search in test name

--------------------------

### Coverage Application in Testing
- sudo docker exec -it apiv1 coverage run --source='.' manage.py test
- sudo docker exec -it apiv1 coverage report

--------------------------

## Show log On Command Line
- sudo docker-compose logs --tail 50 apiv1

--------------------------

Some Product:

{
  "title": "Classic Watch",
  "description": "Silver handle with golden battery and blue screen!",
  "photo": "https://images.unsplash.com/photo-1523170335258-f5ed11844a49?q=80&w=2960&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
  "price": "124.50"
}

{
  "title": "Smart Watch",
  "description": "Black strape, waterproof and super smart!",
  "photo": "https://images.unsplash.com/photo-1638450566735-ead9df13ef3f?q=80&w=2942&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
  "price": "200.00"
}

{
  "title": "Jaelynn Castillo",
  "description": "Stylish unisex watch with leather light brown strap!",
  "photo": "https://images.unsplash.com/photo-1524592094714-0f0654e20314?q=80&w=2899&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
  "price": "150.00"
}

{
  "title": "Wrist Watch",
  "description": "Super cool watch with black strap and androind operation system!",
  "photo": "https://plus.unsplash.com/premium_photo-1681337000010-57e7355ef880?q=80&w=2940&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
  "price": "170.50"
}

