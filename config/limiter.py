from functools import wraps
from inspect import getcallargs
from django.core.cache import cache
from django.conf import settings
import logging

LIMIT_EMAIL_PER_MINUTE = settings.LIMIT_EMAIL_PER_MINUTE

DAY_SECONDS = 86400  # 24*60*60 -> 1 day
HOUR_SECONDS = 3600  # 60*60 -> 1 hour

logger = logging.getLogger('project.ratelimit.alert')


def login_limiter_decorator(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        arguments = getcallargs(func, *args, **kwargs)
        phone_number = arguments.get("email")
        if phone_number:
            added = cache.add(f"RATE_LIMIT_EMAIL_{phone_number}", 1, 60)
            if added:
                count = 1
            else:
                count = cache.incr(f"RATE_LIMIT_EMAIL_{phone_number}")
            if count >= LIMIT_EMAIL_PER_MINUTE:
                return
    return decorator